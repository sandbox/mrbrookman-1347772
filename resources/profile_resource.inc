<?php

/**
 * @file
 * @todo Create file description.
 */

/**
 * Creates a new profile based on the submitted values.
 * 
 * @param $profile
 *   Array representing the attributes of a specific profile type.
 * @param $type
 *   profile type.
 * @return
 *   The ID of the new profile on success, 0 otherwise.
 */
function _profile_resource_create($profile) {
  // @todo Might have to do this the other way around and use the initialised
  // $profile object and populate it with data from $profile.
	$new_profile = profile2_save($profile);
	// Load the newly created profile to make sure everything is all right.
	$result_load = profile2_load($new_profile->pid);

	return ($result_load) ? $profile->pid : 0;
}

/**
 * Returns the result of commerce_profile_load() for the specified profile.
 *
 * @param $pid
 *   The ID of the profile we want to return.
 * @return
 *   The profile object or FALSE if not found.
 *
 * @see commerce_profile_load()
 * @see node_load()
 */
function _profile_resource_retrieve($pid) {
  $profile = profile2_load($pid, $reset = FALSE);
  
  if ($profile) {
    $uri = entity_uri('profile2', $profile);
    $profile->path = url($uri['path'], array('absolute' => TRUE));
    unset($profile->uri);
  }
  return $profile;
}

function _profile_resource_update($pid, $profile) {
  
 return false;
}

/**
 * Deletes a profile.
 *
 * @param $pid
 *   The ID of the profile to delete.
 * @return
 *   TRUE on success, FALSE otherwise.
 * @see commerce_profile_delete()
 */
function _profile_resource_delete($pid) {
  return profile2_delete($pid);
}

function _profile_resource_access($op) {
  // @todo Delete the whole body of the function and use
  // commerce_entity_access().
	global $user;
	$access = FALSE;

	switch ($op) {
		case 'view':
			$access = user_access('');
			break;
		case 'create':
			$access = user_access('');
			break;
		case 'update':
			$access = user_access('');
			break;
		case 'delete':
			$access = user_access('');
			break;
	}
	$access = TRUE;// remove when access function is finished, just for testing

	return $access;
}
